package nz.co.basiccalculator;

import android.app.Application;

import com.google.inject.Injector;
import com.google.inject.Stage;

import roboguice.RoboGuice;

/**
 * BasicCalculator application
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setApplicationInjector(this);
    }

    /**
     * Sets the application injector. Using the {@link RoboGuice#newDefaultRoboModule} as well as a
     * custom binding module {@link MainModule} to set up your application module
     * @param application
     * @return
     */
    public static Injector setApplicationInjector(Application application) {
        return RoboGuice.setBaseApplicationInjector(application, Stage.DEVELOPMENT, RoboGuice.newDefaultRoboModule
                (application), new MainModule());
    }
}

