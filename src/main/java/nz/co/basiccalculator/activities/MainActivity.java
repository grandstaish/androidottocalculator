package nz.co.basiccalculator.activities;

import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import nz.co.basiccalculator.R;
import nz.co.basiccalculator.core.Constants;
import nz.co.basiccalculator.core.EqualsEvent;
import nz.co.basiccalculator.core.HistoryItem;
import roboguice.inject.ContentView;

import java.text.DecimalFormat;
import java.util.ArrayList;

@ContentView(R.layout.main)
public class MainActivity extends RoboSherlockFragmentActivity {

    private ArrayList<HistoryItem> results;

    @Inject
    private Bus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
        if (savedInstanceState != null) {
            results = (ArrayList<HistoryItem>)savedInstanceState.getSerializable("results");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    public void putResult(HistoryItem result) {
        if (results == null) {
            results = new ArrayList<HistoryItem>();
        }
        results.add(result);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEqualsEvent(EqualsEvent event) {

        // Format the result
        DecimalFormat df = new DecimalFormat(Constants.NUMBER_FORMAT);
        String formattedResult = df.format(event.getAmount());

        putResult(new HistoryItem(formattedResult));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.history:
                final Intent i = new Intent(this, HistoryActivity.class);
                i.putExtra("results", results);
                startActivityForResult(i, Constants.HISTORY_ACTIVITY_REQUEST_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.HISTORY_ACTIVITY_REQUEST_CODE:
                switch (resultCode) {
                    case Constants.CLEAR_HISTORY:
                        results = null;
                        break;
                }
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("results", results);
    }
}
