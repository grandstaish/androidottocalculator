package nz.co.basiccalculator.activities;

import android.os.Bundle;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import nz.co.basiccalculator.R;
import nz.co.basiccalculator.core.Constants;
import nz.co.basiccalculator.core.HistoryItem;
import roboguice.inject.ContentView;

import java.util.ArrayList;

@ContentView(R.layout.history)
public class HistoryActivity extends RoboSherlockFragmentActivity {

    public ArrayList<HistoryItem> getResults() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return (ArrayList< HistoryItem>)extras.getSerializable("results");
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.history_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.clearHistory:
                setResult(Constants.CLEAR_HISTORY);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
