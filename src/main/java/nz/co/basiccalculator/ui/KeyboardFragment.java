package nz.co.basiccalculator.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import nz.co.basiccalculator.R;
import nz.co.basiccalculator.core.CalculatorBrain;
import roboguice.inject.InjectView;

public class KeyboardFragment extends RoboSherlockFragment {

    @Inject private CalculatorBrain brain;

    @InjectView(R.id.buttonNum0) protected Button b0;
    @InjectView(R.id.buttonNum1) protected Button b1;
    @InjectView(R.id.buttonNum2) protected Button b2;
    @InjectView(R.id.buttonNum3) protected Button b3;
    @InjectView(R.id.buttonNum4) protected Button b4;
    @InjectView(R.id.buttonNum5) protected Button b5;
    @InjectView(R.id.buttonNum6) protected Button b6;
    @InjectView(R.id.buttonNum7) protected Button b7;
    @InjectView(R.id.buttonNum8) protected Button b8;
    @InjectView(R.id.buttonNum9) protected Button b9;
    @InjectView(R.id.buttonPeriod) protected Button bPeriod;
    @InjectView(R.id.buttonPlus) protected Button bPlus;
    @InjectView(R.id.buttonMinus) protected Button bMinus;
    @InjectView(R.id.buttonEquals) protected Button bEquals;
    @InjectView(R.id.buttonClear) protected Button bClear;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.keyboard, null, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        setNumberPressClickListeners();
        setModifierPressClickListeners();
        setEqualsPressClickListener();
        setClearPressClickListener();
    }

    private void setNumberPressClickListeners() {

        View.OnClickListener numberPressClickLister = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v instanceof Button) {
                    Button b = (Button)v;
                    brain.appendNumberToCurrentNumber(b.getText().toString());
                }
            }
        };

        b0.setOnClickListener(numberPressClickLister);
        b1.setOnClickListener(numberPressClickLister);
        b2.setOnClickListener(numberPressClickLister);
        b3.setOnClickListener(numberPressClickLister);
        b4.setOnClickListener(numberPressClickLister);
        b5.setOnClickListener(numberPressClickLister);
        b6.setOnClickListener(numberPressClickLister);
        b7.setOnClickListener(numberPressClickLister);
        b8.setOnClickListener(numberPressClickLister);
        b9.setOnClickListener(numberPressClickLister);
        bPeriod.setOnClickListener(numberPressClickLister);
    }

    private void setModifierPressClickListeners() {

        View.OnClickListener modifierPressClickLister = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v instanceof Button) {
                    Button b = (Button)v;
                    brain.applyModifier(b.getText().toString());
                }
            }
        };

        bPlus.setOnClickListener(modifierPressClickLister);
        bMinus.setOnClickListener(modifierPressClickLister);
    }

    private void setEqualsPressClickListener() {

        bEquals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brain.calculate();
            }
        });
    }

    private void setClearPressClickListener() {

        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brain.clear();
            }
        });
    }
}
