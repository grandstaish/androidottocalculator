package nz.co.basiccalculator.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import nz.co.basiccalculator.R;
import nz.co.basiccalculator.core.ClearEvent;
import nz.co.basiccalculator.core.Constants;
import nz.co.basiccalculator.core.EqualsEvent;
import nz.co.basiccalculator.core.InputEvent;
import roboguice.inject.InjectView;

import java.text.DecimalFormat;

public class OutputFragment extends RoboSherlockFragment {

    private final static String TAG = "OutputFragment";
    @Inject private Bus bus;

    @InjectView(R.id.output) private TextView outputView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.output, null, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            outputView.setText(savedInstanceState.getString("currentDisplay"));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onInputEvent(InputEvent event) {
        outputView.setText(event.getInput());
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onClearEvent(ClearEvent event) {
        outputView.setText("");
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEqualsEvent(EqualsEvent event) {

        // Format the result
        DecimalFormat df = new DecimalFormat(Constants.NUMBER_FORMAT);
        String formattedResult = df.format(event.getAmount());

        // Display the result
        outputView.setText(formattedResult);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("currentDisplay", outputView.getText().toString());
    }
}
