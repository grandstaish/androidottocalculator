package nz.co.basiccalculator.ui;

import android.view.LayoutInflater;

import com.github.kevinsawicki.wishlist.SingleTypeAdapter;
import nz.co.basiccalculator.R;

import java.util.List;

/**
 * List adapter that colors rows in alternating colors
 *
 * @param <V>
 */
public abstract class AlternatingColorListAdapter<V> extends SingleTypeAdapter<V> {

    private final int primaryResource;
    private final int secondaryResource;

    /**
     * Create adapter with alternating row colors
     *
     * @param layoutId
     * @param inflater
     * @param items
     */
    public AlternatingColorListAdapter(final int layoutId, final LayoutInflater inflater, final List<V> items) {
        super(inflater, layoutId);

        primaryResource = R.drawable.table_background_selector;
        secondaryResource = R.drawable.table_background_alternate_selector;

        setItems(items);
    }

    @Override
    protected void update(final int position, final V item) {
        if (position % 2 != 0) {
            updater.view.setBackgroundResource(primaryResource);
        } else {
            updater.view.setBackgroundResource(secondaryResource);
        }
    }
}
