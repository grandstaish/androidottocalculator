package nz.co.basiccalculator.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import nz.co.basiccalculator.R;
import nz.co.basiccalculator.activities.HistoryActivity;

public class HistoryFragment extends RoboSherlockFragment {

    /**
     * List view
     */
    protected ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.history_list, null, false);
    }

    /**
     * Detach from list view.
     */
    @Override
    public void onDestroyView() {
        listView = null;
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = (ListView) view.findViewById(android.R.id.list);

        HistoryActivity activity = (HistoryActivity)getActivity();
        listView.setAdapter(new HistoryListAdapter(getActivity().getLayoutInflater(), activity.getResults()));
    }
}
