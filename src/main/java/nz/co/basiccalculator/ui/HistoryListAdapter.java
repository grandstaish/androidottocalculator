package nz.co.basiccalculator.ui;

import android.view.LayoutInflater;
import nz.co.basiccalculator.R;
import nz.co.basiccalculator.core.HistoryItem;

import java.util.List;

public class HistoryListAdapter extends AlternatingColorListAdapter<HistoryItem> {

    /**
     * @param inflater
     * @param items
     */
    public HistoryListAdapter(LayoutInflater inflater, List<HistoryItem> items) {
        super(R.layout.history_list_item, inflater, items);
    }

    @Override
    protected int[] getChildViewIds() {
        return new int[] { R.id.result };
    }

    @Override
    protected void update(int position, HistoryItem item) {
        super.update(position, item);
        setText(0, item.getResult());
    }
}
