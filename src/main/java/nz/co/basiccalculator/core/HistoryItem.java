package nz.co.basiccalculator.core;

import java.io.Serializable;

public class HistoryItem implements Serializable {

    private String result;

    public HistoryItem(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
