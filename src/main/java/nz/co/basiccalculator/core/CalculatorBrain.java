package nz.co.basiccalculator.core;

import com.google.inject.Inject;
import com.squareup.otto.Bus;

/**
 * Stores the input and performs the calculations
 */
public class CalculatorBrain {

    @Inject private Bus bus;

    /**
     * The current number being input
     */
    private String currentNum = null;

    /**
     * The last modifier that was pressed
     */
    private String currentModifier = null;

    /**
     * The last calculated number
     */
    private Double storedNum = null;

    /**
     * When the user presses a number or a period
     * @param number
     */
    public void appendNumberToCurrentNumber(String number) {
        if (currentNum != null) {

            // If the input is a period and a period already exists, then return
            if (number.equals(".") && currentNum.contains(".")) {
                return;
            }

            // Otherwise just append the input
            currentNum += number;

        } else {

            // If the user inputs a period first, just prepend a 0 in-front to make it look cleaner
            if (number.equals(".")) {
                currentNum = "0.";
            } else {
                currentNum = number;
            }

            // If the user has no modifier and no current number, then delete the stored number
            // because the user wants to start from scratch. This will occur after the user presses
            // equals, then presses a number (without pressing another modifier)
            if (currentModifier == null) {
                storedNum = null;
            }
        }
        bus.post(new InputEvent(currentNum));
    }

    /**
     * When the user presses a modifier
     * @param modifier
     */
    public void applyModifier(String modifier) {

        if (modifier != null) {

            // Check for supported modifiers
            if (modifier.equals("+") || modifier.equals("-")) {

                // Set current modifier
                currentModifier = modifier;

                // If the user has typed a number and hit a modifier, it is handy to
                // show them the current result and display it immediately.
                if (currentNum != null) {
                    calculate(false);
                }
            }
        }
    }

    /**
     * When the user presses the equals key
     */
    public void calculate() {
        calculate(true);
    }

    private void calculate(boolean clearModifier) {

        // This operation can only be performed if we have a current number, a current
        // modifier, and the current number is valid. This if-statement ensures all 3.
        if (currentNum != null && currentModifier != null && !currentNum.endsWith(".")) {

            // Stores our result
            Double answer;

            // If this is an on-going calculation, continue it. Otherwise we can just set
            // the answer to be the current number
            if (storedNum != null) {
                answer = calculate(storedNum, Double.parseDouble(currentNum), currentModifier);
            } else {
                answer = Double.parseDouble(currentNum);
            }

            // Post the answer event for any interested parties
            bus.post(new EqualsEvent(answer));

            // Store the result for on-going calculations
            storedNum = answer;

            // Clear the current number, as there is none
            currentNum = null;

            // It only makes sense to remember the modifier when calculating immediately after
            // setting it. If the user presses equals, the current modifier should not be
            // remembered
            if (clearModifier) {
                currentModifier = null;
            }
        }
    }

    private double calculate(double i1, double i2, String modifier) {

        // Arithmetic
        if (modifier.equals("+")) {
            return i1 + i2;
        } else if (modifier.equals("-")) {
            return i1 - i2;
        } else {
            return 0;
        }
    }

    /**
     * When the user presses the clear key
     */
    public void clear() {

        // Destroy everything
        storedNum = null;
        currentNum = null;
        currentModifier = null;

        // Post the clear event for any interested parties
        bus.post(new ClearEvent());
    }
}
