package nz.co.basiccalculator.core;

public class EqualsEvent {

    private Double amount;

    public EqualsEvent(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }
}
