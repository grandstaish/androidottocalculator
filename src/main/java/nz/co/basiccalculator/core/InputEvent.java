package nz.co.basiccalculator.core;

public class InputEvent {

    private String input;

    public InputEvent(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }
}
