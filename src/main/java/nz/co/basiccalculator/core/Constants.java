package nz.co.basiccalculator.core;

public class Constants {

    public static final String NUMBER_FORMAT = "#.#####";

    // Custom request codes
    public static final int HISTORY_ACTIVITY_REQUEST_CODE = 100;

    // Custom result codes
    public static final int CLEAR_HISTORY = 1000;
}
